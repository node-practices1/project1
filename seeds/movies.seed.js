const mongoose = require("mongoose");
const Movie = require("../models/Movie");
const { DB_URL, CONFIG_DB } = require("../config/db");

// console.log(DB_URL);
// console.log(CONFIG_DB);

const moviesArray = [
  {
    title: "The Matrix",
    director: "Hermanas Wachowski",
    year: 1999,
    genre: "Acción",
  },
  {
    title: "The Matrix Reloaded",
    director: "Hermanas Wachowski",
    year: 2003,
    genre: "Acción",
  },
  {
    title: "Buscando a Nemo",
    director: "Andrew Stanton",
    year: 2003,
    genre: "Animación",
  },
  {
    title: "Buscando a Dory",
    director: "Andrew Stanton",
    year: 2016,
    genre: "Animación",
  },
  {
    title: "Interestelar",
    director: "Christopher Nolan",
    year: 2014,
    genre: "Ciencia ficción",
  },
  {
    title: "50 primeras citas",
    director: "Peter Segal",
    year: 2004,
    genre: "Comedia romántica",
  },
];

mongoose
  .connect(DB_URL, CONFIG_DB)
  .then(async () => {
    console.log("Conectado a BBDD -> Ejecutando seed de Movies");

    const allMovies = await Movie.find();
    if (allMovies.length) {
      await Movie.collection.drop();
      console.log("Colección eliminada");
    }
  })
  .catch((error) => console.log("Error buscando", error))
  .then(async () => {
    await Movie.insertMany(moviesArray);
    console.log("Películas añadidas");
  })
  .catch((error) => console.log("Error añadiendo las peliculas", error))
  .finally(() => mongoose.disconnect());
