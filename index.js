const express = require("express");
const movieRouter = require('./routes/movie.routes')

const { connectToDb } = require("./config/db");
connectToDb();



const PORT = 3000;
const server = express();

const router = express.Router();

router.get("/", (req, res) => {
  return res.send("Server Working!");
});

//   router.get("/movies", (req, res) => {
//     return res.send("Estoy accediendo a la pelicula");
//   });

// router.get("/movies", async (req, res) => {
//   try {
//     const movies = await Movie.find();
//     return res.status(200).json(movies);
//   } catch (error) {
//     return res.status(500).json(error);
//   }
// });

// router.get("/movies/:id", async (req, res) => {
//   try {
//     const id = req.params.id;
//     const movie = await Movie.findById(id);

//     if (movie) {
//       return res.status(200).json(movie);
//     } else {
//       return res.status(404).json("No tenemos esa pelicula");
//     }
//   } catch (error) {
//     return res.status(500).json(error);
//   }
// });
// router.get("/movies/title/:title", async (req, res) => {
//   try {
//     const title = req.params.title;
//     const movie = await Movie.find({ title });
//     // console.log('Pelicula ->', movie);
//     return res.status(200).json(movie);
//   } catch (error) {
//     return res.status(500).json(error);
//   }
// });
// router.get("/movies/genre/:genre", async (req, res) => {
//   try {
//     const genre = req.params.genre;
//     const movie = await Movie.find({ genre });

//     return res.status(200).json(movie);
//   } catch (error) {
//     return res.status(500).json(error);
//   }
// });
// router.get("/movies/year/:year", async (req, res) => {
//   try {
//     const year = req.params.year;
//     const movie = await Movie.find({ year: { $gt: 2010 } });

//     return res.status(200).json(movie);
//   } catch (error) {
//     return res.status(500).json(error);
//   }
// });

server.use("/", router);
server.use('/movies', movieRouter)

// server.use("/create-matrix", async (req, res) => {
//   try {
//     const newMovie = new Movie({
//       title: "The Matrix",
//       director: "Hermanas Wachowski",
//       year: 1999,
//       genre: "Acción",
//     });

//     const movie = await newMovie.save();
//     console.log(movie);

//     return res.send(movie);
//   } catch (error) {
//     console.log("Error creando la película");
//   }
// });

// server.use("/", (req, res) => {
//   res.send("Server Working!");
// });

const serverCallback = () => {
  console.log(`Servidor conectado en http://localhost:${PORT}`);
};

server.listen(PORT, serverCallback);

// const indexRouter = require ('./routes/index.routes');
// const movieRouter = require ('./routes/movies.routes');

// const {connect} = require("./config/db");
// connect();

// server.use('/', indexRouter);
// server.use('/movies', movieRouter);

// const serverCallback = () => {
//   console.log(`Servidor con hambre en http://localhost:${PORT}`);
// }

// server.listen(PORT, serverCallback);
// server.use('/', (req, res) => {
//     res.send('Servidor conectado');
// });
