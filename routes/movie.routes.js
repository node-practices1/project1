const express = require('express');
const Movie = require("../models/Movie");

const router = express.Router();

router.get("/", async (req, res) => {
    try {
      const movies = await Movie.find();
      return res.status(200).json(movies);
    } catch (error) {
      return res.status(500).json(error);
    }
  });
  
  router.get("/:id", async (req, res) => {
    try {
      const id = req.params.id;
      const movie = await Movie.findById(id);
  
      if (movie) {
        return res.status(200).json(movie);
      } else {
        return res.status(404).json("No tenemos esa pelicula");
      }
    } catch (error) {
      return res.status(500).json(error);
    }
  });
  router.get("/title/:title", async (req, res) => {
    try {
      const title = req.params.title;
      const movie = await Movie.find({ title });
      // console.log('Pelicula ->', movie);
      return res.status(200).json(movie);
    } catch (error) {
      return res.status(500).json(error);
    }
  });
  router.get("/genre/:genre", async (req, res) => {
    try {
      const genre = req.params.genre;
      const movie = await Movie.find({ genre });
  
      return res.status(200).json(movie);
    } catch (error) {
      return res.status(500).json(error);
    }
  });
  router.get("/year/:year", async (req, res) => {
    try {
      const year = req.params.year;
      const movie = await Movie.find({ year: { $gt: year } });
  
      return res.status(200).json(movie);
    } catch (error) {
      return res.status(500).json(error);
    }
  });

  module.exports = router;